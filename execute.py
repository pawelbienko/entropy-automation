#! /usr/bin/python

from subprocess import check_output
import re
import csv

for x in range(0, 2):
	entropyFile = open('results/entropy/' + str(x) + '.csv', 'w')
	agent1File = open('results/agent1/'+ str(x) +'.csv', 'w')
	agent2File = open('results/agent2/'+ str(x) +'.csv', 'w')

	for line in check_output(
		[
			"java", 
			"-classpath", 
			"entropy.jar:lib/jfreechart-1.0.19.jar:lib/jcommon-1.0.23.jar:lib/jade.jar", 
			"jade.Boot", 
			"-gui", 
			"world:com.entropy.WorldAgent(12);chiter:com.entropy.ChiterGuestAgent(200,12,1,null);good:com.entropy.GoodGuestAgent(200,12,1,null);"
		]
	).splitlines():
		print "%r" % line
		searchEntropy = re.search(r"dispatcher entropy:.+(\d+\.\d+)", line)
		searchAgent1 = re.search(r"Agent1 effort:.(\d+) Agent1 elapsedTime:.(\d+\.\d+)", line)
		searchAgent2 = re.search(r"Agent2 effort:.(\d+) Agent2 elapsedTime:.(\d+\.\d+)", line)
		
		if searchEntropy is not None:
			if searchEntropy.group(1) != "":
				writer = csv.writer(entropyFile)
				writer.writerow([searchEntropy.group(1).replace('.',',')])

		if searchAgent1 is not None:
			if searchAgent1.group(1) != "":
				writer = csv.writer(agent1File)
				writer.writerow([searchAgent1.group(1).replace('.',','), searchAgent1.group(2).replace('.',',')])
				
		if searchAgent2 is not None:
			if searchAgent2.group(1) != "":
				writer = csv.writer(agent2File)
				writer.writerow([searchAgent2.group(1).replace('.',','), searchAgent2.group(2).replace('.',',')])		

	entropyFile.close()
	agent1File.close()
	agent2File.close()
